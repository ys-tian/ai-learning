import requests
import json
import time
import csv
import os
import argparse

get_video_url = 'https://space.bilibili.com/ajax/member/getSubmitVideos'
# 获取评论url
get_reply_url = 'https://api.bilibili.com/x/v2/reply'
tmp_path = os.getcwd() + '/../tmp'


def get_video(mid, tid):
    """
    获取作者所有视频列表，按照发布时间倒序排
    :param mid: 作者id
    :param tid: 视频分类，160表示生活
    :return:
    """
    filename = tmp_path + '/' + str(mid) + '.csv'
    prepare_csv_file(filename)
    result = []
    page = 1
    with open(filename, 'w+') as f:
        writer = csv.writer(f)
        header = True
        while True:
            params_tmp = {'mid': mid, 'tid': tid, 'pageSize': 20, 'page': page, 'keyword': '', 'order': 'pubdate'}
            # print(params_tmp)
            resp_tmp = requests.get(get_video_url, params_tmp)
            resp_tmp_json = json.loads(resp_tmp.content)
            # print("resp_tmp_json", resp_tmp_json)
            if resp_tmp_json is None or resp_tmp_json['status'] is False or len(resp_tmp_json['data']['vlist']) == 0:
                break
            for item in resp_tmp_json['data']['vlist']:
                result.append(item)
                if header:
                    keys_view = item.keys()
                    keys_tmp = []
                    for item_key in keys_view:
                        keys_tmp.append(item_key)
                    writer.writerow(keys_tmp)
                    header = False
                value_view = item.values()
                value_tmp = []
                for item_value in value_view:
                    value_tmp.append(item_value)
                writer.writerow(value_tmp)
            page += 1
            time.sleep(0.5)
    return result


def get_comment_and_danmu(mid, vlist, isnewfile=True):
    """
    获取评论，写入文件
    :param mid: user id
    :param vlist: 视频列表
    :param isnewfile: 是否新建文件
    :return:
    """
    filename = tmp_path + '/' + str(mid) + '_reply.csv'
    if isnewfile:
        prepare_csv_file(filename)
    with open(filename, 'a') as f:
        writer = csv.writer(f)
        writer.writerow(['aid', 'uid', 'uname', 'sex', 'sign', 'level', 'medal_name', 'medal_id', 'medal_level',
                         'reply', 'like'])
        for item in vlist:
            pn = 1
            while True:
                print(pn)
                # print(item)
                # print(type(item))
                params_tmp = {'jsonp': 'jsonp', 'pn': pn, 'type': 1, 'sort': 0, 'oid': item['aid'],
                              '_': int(time.time() * 1000)}
                # print(params_tmp)
                result_tmp = requests.get(get_reply_url, params_tmp)
                # print(result_tmp.content)
                result_json_tmp = json.loads(result_tmp.content)
                if result_json_tmp['data']['replies'] is None:
                    break
                for item2 in result_json_tmp['data']['replies']:
                    row_tmp = [str(item['aid']), str(item2['member']['mid']), item2['member']['uname'],
                               item2['member']['sex'], item2['member']['sign'],
                               item2['member']['level_info']['current_level'], item2['content']['message'],
                               item2['like']]
                    if item2['member']['fans_detail'] is not None:
                        row_tmp.insert(6, item2['member']['fans_detail']['medal_name'])
                        row_tmp.insert(7, item2['member']['fans_detail']['medal_id'])
                        row_tmp.insert(8, item2['member']['fans_detail']['level'])
                    else:
                        row_tmp.insert(6, '')
                        row_tmp.insert(7, '')
                        row_tmp.insert(8, '')
                    print(row_tmp)
                    writer.writerow(row_tmp)
                pn += 1
                r_count = result_json_tmp['data']['page']['count']
                r_size = result_json_tmp['data']['page']['size']
                if pn > (r_count / r_size + 1):
                    break
                time.sleep(0.5)


def prepare_csv_file(filename):
    """
    删除之前的文件
    :param filename:
    :return:
    """
    if os.path.exists(filename):
        os.remove(filename)


def get_vlist_from_file(filename):
    """

    :param filename:
    :return:
    """
    with open(filename, 'r') as f:
        reader = csv.DictReader(f)
        for item in reader:
            # print(type(item))
            # print(item)
            # print(item['comment'])
            vlist_tmp.append(item)
            #     break
            # count += 1


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('--mid', help='up主id', required=True)
    arg = ap.parse_args()
    mid = arg.mid

    vlist_tmp = get_video(mid=mid, tid=160)
    # print(result)
    get_comment_and_danmu(mid=mid, vlist=vlist_tmp, isnewfile=True)


