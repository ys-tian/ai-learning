import numpy as np
import pandas as pd
import requests
import time
import matplotlib

font_hei = matplotlib.font_manager.FontProperties(
    fname=r"SimHei.ttf", size=15)  # 设置中文字体


def get_data(base_url, page):
    """
    获取B站单个页面的视频信息
    base_url: json的api
    page: 页数
    return 一个二维的list,16列
    """
    url = base_url.format(str(page))  # 得到对应的url
    response = requests.get(url, headers=header)  # 发出请求，获取回应
    videos = pd.read_json(response.text[37:-1]).iloc[:, 3].iloc[0]  # 获取列表
    videos_list = []  # 用来储存数据
    for video in videos:  # 循环每个番剧
        video_inf = []  # 用来储存每个番剧的信息
        video_inf.append(video['stat']['aid'])  # av号
        video_inf.append(video["title"])  # 标题
        video_inf.append(video['owner']["name"])  # 上传者
        video_inf.append(time.ctime(video['pubdate']))  # 上传时间

        video_inf.append(video['stat']['view'])  # 观看人数
        video_inf.append(video['stat']['danmaku'])  # 弹幕数
        video_inf.append(video['stat']['reply'])  # 评论数量
        video_inf.append(video['stat']['favorite'])  # 收藏数量
        video_inf.append(video['stat']['coin'])  # 投币数
        video_inf.append(video['stat']['share'])  # 分享数
        video_inf.append(video['stat']['like'])  # 点喜欢的人数
        video_inf.append(video['stat']['dislike'])  # 点不喜欢的人数
        video_inf.append(video['stat']["now_rank"])  # 排名
        video_inf.append(video['stat']["his_rank"])  # 排名

        video_inf.append(video['tname'])  # 标签
        video_inf.append(video['desc'])  # 描述

        videos_list.append(video_inf)
    return videos_list
